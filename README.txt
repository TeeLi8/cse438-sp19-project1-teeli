In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

General information:

   For user input, I did not check for the boundary for calories in my code;
   for example I did not check whether the input is positive or not.
   However in xml file I specify the input to be of type "number" which actually
   does not allow not-a-number input or negative input. I think that's ok.

Creative portion:

   My feature is that by pressing one of the existing items, the app will make a copy
   of that item and add to the list.

   I did this because sometimes maybe when the user says "ok i know the calories for
   one piece of chocolate is 300, and I had 3 pieces, and I don't want to do the math
   myself what can I do", they can just click on that item and create a duplicate of it.

   I add an on-click-listener to the item-list fragment, which will first find the item
   object corresponding to that position in the list fragment, and copy it's value and
   add to the list.
   
1. (10 Points) User can enter total calorie amount on start up
2. (8 Points) User can add new food item by name
3. (8 Points) User can add new food item by calorie
4. (4 Points) Adding new food items is done in a second activity
5. (5 Points) Calories remaining is updated with each new food item
6. (5 Points) Calorie consumed is updated with each new food item
7. (10 Points) The list of food items displays foods and their respective calories amounts
8. (10 Points) Color change when calorie count becomes negative
9. (10 Points) All inputs are filtered and error messages are displayed accordingly
10. (2 Points) Code is clean and commented
11. (3 Points) App is visually appealing
12. (15 Points) Creative portion - design your own feature(s)!
      -10 Creative portion not rigorous enough to warrant 15 points 
      
Total: 80/90 


