package com.example.calorieapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class ItemAdapter(context: Context, id : Int, item_list : ArrayList<Item>) : ArrayAdapter<Item>(context,id,item_list){

    private var currentContext : Context = context
    private var currentList : ArrayList<Item> = item_list

    override fun getCount(): Int {
        //keep at least one slot
        if(currentList.size==0){
            return 1
        }
        return currentList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        //inflate item_row fragment
        val layoutInf = LayoutInflater.from(currentContext).inflate(R.layout.item_row, parent, false)


        val nameView = layoutInf.findViewById<TextView>(R.id.itemName)
        val calView = layoutInf.findViewById<TextView>(R.id.itemCal)

        //display item
        if(currentList.size == 0){
            //place holders when list is empty
            nameView.text = "The list is currently empty"
            calView.text = ""
        }
        else{
            nameView.text = currentList[position].name
            calView.text = currentList[position].cal.toString()
        }


        return layoutInf
    }
}