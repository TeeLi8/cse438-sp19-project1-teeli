package com.example.calorieapp

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){

    private var totalCalorie = 0    //the total calories remaining
    private var totalCalorieConsumed = 0   //the total calories consumed
    private var itemList : ArrayList<Item> = arrayListOf()  //list that hold items
    private lateinit var aAdapter : ItemAdapter      //adapter

    companion object {
        const val tot_req_code = 0
        const val add_item_code = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        aAdapter = ItemAdapter(this,R.layout.activity_main,itemList)
        item_list.adapter = aAdapter
        button.setOnClickListener{ addItem() }

        item_list.setOnItemClickListener{parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
            createDuplicate(position)
        }

        toSubmit()  //ask user for total calories when started
    }

    //update UI as total calories changes
    private fun setTotalCal(tot : Int, totConsumed : Int){
        totCalText.text = "$tot"
        tot_cal_val.text= "$totConsumed"
        if(tot<=0){
            totCalText.setTextColor(getColor(R.color.colorAccent))
        }
    }

    //start the activity that add an item
    private fun addItem(){
        val intent = Intent(this@MainActivity, AddItemActivity::class.java)
        startActivityForResult(intent, add_item_code)
    }

    //start the activity that ask for total calories
    private fun toSubmit(){
        val intent = Intent(this@MainActivity, AddActivity::class.java)
        startActivityForResult(intent, tot_req_code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == tot_req_code){
            //the result from the activity that get total calories
            if(resultCode == Activity.RESULT_OK){
                val totCal : String? = data?.getStringExtra(AddActivity.input_name)

                totalCalorie = totCal!!.toInt()
                setTotalCal(totalCalorie,0)

            }
        }
        else if(requestCode == add_item_code){
            //the result from the activity that get new items
            if(resultCode == Activity.RESULT_OK){
                val newName : String? = data?.getStringExtra(AddItemActivity.input_itemName)
                val newCal : Int? = data?.getIntExtra(AddItemActivity.input_itemCal,0)

                //create new item and add to list
                val newItem = Item(newName!!,newCal!!)
                itemList.add(newItem)

                //update adapter
                aAdapter = ItemAdapter(this,R.layout.activity_main,itemList)
                item_list.adapter = aAdapter

                //update UI information
                totalCalorie-=newCal
                totalCalorieConsumed+=newCal
                setTotalCal(totalCalorie,totalCalorieConsumed)


            }
        }
    }

    //The call back function that copy the item after it is pressed
    private fun createDuplicate(position : Int){
        if(itemList.size>0){
            val dupItem : Item = itemList[position]
            itemList.add(dupItem)
            aAdapter = ItemAdapter(this,R.layout.activity_main,itemList)
            item_list.adapter = aAdapter

            //update UI information
            totalCalorie-=dupItem.cal
            totalCalorieConsumed+=dupItem.cal
            setTotalCal(totalCalorie,totalCalorieConsumed)
        }

    }

}
