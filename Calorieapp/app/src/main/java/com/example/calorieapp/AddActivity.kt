package com.example.calorieapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_add.*


class AddActivity : AppCompatActivity() {

    //This is the activity that ask for the total calories

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        submit_button.setOnClickListener { submit() }
    }

    private fun submit(){
        val input : String? = calorie.text.toString()

        //throw a toast if the user left the input box empty
        //otherwise return to main activity with user's input
        if(input==null||input==""){
            val toast = Toast.makeText(this@AddActivity, "Please enter a number", Toast.LENGTH_SHORT)
            toast.show()
        }
        else{
            val retIntent = Intent()
            retIntent.putExtra(input_name, input)
            setResult(Activity.RESULT_OK, retIntent)
            finish()
        }

    }

    companion object {
        const val input_name  = "com.example.calorieapp.Input"
    }

}
