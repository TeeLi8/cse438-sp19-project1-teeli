package com.example.calorieapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_add_item.*


class AddItemActivity : AppCompatActivity() {

    //This activity is for adding new items

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        add_button.setOnClickListener{submit()}
    }

    private fun submit(){
        val inputName : String? = add_food.text.toString()
        val inputCal : String? = add_cal.text.toString()

        //throw a toast if the user left one of the input field empty
        //otherwise return to main activity with user's input

        if(inputName==null||inputName==""||inputCal==null||inputCal==""){
            val toast = Toast.makeText(this@AddItemActivity, "Fields cannot be empty", Toast.LENGTH_SHORT)
            toast.show()
        }
        else{
            val inputCalInt : Int = inputCal.toInt()
            val retIntent = Intent()
            retIntent.putExtra(input_itemName, inputName)
            retIntent.putExtra(input_itemCal,inputCalInt)
            setResult(Activity.RESULT_OK, retIntent)
            finish()
        }

    }

    companion object {
        const val input_itemName  = "com.example.calorieapp.InputItemName"
        const val input_itemCal  = "com.example.calorieapp.InputItemCal"
    }

}
